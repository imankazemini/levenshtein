package com.dac.mobile.Quoori;

public class TestCaseModel {

   private String token1 ="";
   private String token2 = "";
   private int maximum = Integer.MAX_VALUE;
   private int expectReturn = Integer.MAX_VALUE;
   private int expectReturnWithMax = Integer.MAX_VALUE;


    public TestCaseModel(String token1, String token2, int maximum, int expectReturn, int expectReturnWithMax) {
        this.token1 = token1;
        this.token2 = token2;
        this.maximum = maximum;
        this.expectReturn = expectReturn;
        this.expectReturnWithMax = expectReturnWithMax;
    }

    public String getToken1() {
        return token1;
    }

    public void setToken1(String token1) {
        this.token1 = token1;
    }

    public String getToken2() {
        return token2;
    }

    public void setToken2(String token2) {
        this.token2 = token2;
    }


    public int getMaximum() {
        return maximum;
    }

    public void setMaximum(int maximum) {
        this.maximum = maximum;
    }


    public int getExpectReturn() {
        return expectReturn;
    }

    public void setExpectReturn(int expectRetunr) {
        this.expectReturn = expectRetunr;
    }

    public int getExpectReturnWithMax() {
        return expectReturnWithMax;
    }

    public void setExpectReturnWithMax(int expectReturnWithMax) {
        this.expectReturnWithMax = expectReturnWithMax;
    }
}
