package com.dac.mobile.Quoori;
import com.dac.mobile.Quoori.TestCaseModel;

import java.util.ArrayList;

public class LevenShtein {




    /* This is the answer of code challenge from Quoori.com and is answered by Iman Kazemini
     * Note: This question has 5 parts. The answer to each part has marked by note.
     * For Question Five: This question is about performance measurement. we have 2 different
     * Levenshtein algorithm and also we have two different types of performance measurement: 1- memory 2- speed
     * Thus, we have four different methods to calculate each of them. Also, we have complexity calculation above each of Levenshtein algorithms
     *
     */


    //Question One
    //Complexity:  BestCase: 1  AverageCase: N^2   WorstCase: N^2
    private int OneColumnLevenShteinDistance(String token1, String token2) {
        //return for zero length
        if (token1.length() == 0)
            return token2.length();
        if (token2.length() == 0)
            return token1.length();

        // define just one column to reduce space
        int token1Lenght = token1.length();
        int[] column = new int[token1Lenght + 1];

        int index2 = 0;
        char ch2 = token2.charAt(index2);
        column[0] = 1;

        //Initiate column and comparison base on virtual column
        for (int index1 = 1; index1 <= token1Lenght; index1++) {
            int cost = token1.charAt(index1 - 1) == ch2 ? 0 : 1;
            //Thinkng of a matrix we should calculate the Lowest of three: top (column[index1 - 1]), aboveleft: index1 - 1,
            // left: index1.
            column[index1] = Math.min(column[index1 - 1], index1 - 1) + cost;
        }

        // okay, now we have an initialized first column, and we can
        // compute the rest of a virtual matrix.
        int top = 0;
        for (index2 = 1; index2 < token2.length(); index2++) {
            ch2 = token2.charAt(index2);
            top = index2 + 1;

            int smallest = token1Lenght * 2;
            for (int ix1 = 1; ix1 <= token1Lenght; ix1++) {
                int cost = token1.charAt(ix1 - 1) == ch2 ? 0 : 1;

                int value = Math.min(Math.min(top, column[ix1 - 1]), column[ix1]) +
                        cost;
                column[ix1 - 1] = top;
                top = value;
                smallest = Math.min(smallest, value);
            }
            column[token1Lenght] = top;
        }
        return top;
    }

    //Question Two
    private void runTestsBySingleColumnLevenShtein() {
        ArrayList<TestCaseModel> arrayList = getTestCases();
        for (TestCaseModel test : arrayList) {
            System.out.println("Distance:" + OneColumnLevenShteinDistance(test.getToken1(), test.getToken2()) + "- Expect:" + test.getExpectReturn());
        }
    }

    //Question Three
    //Complexity:  BestCase: 1  AverageCase: N^2   WorstCase: N^2
    private int OneColumnLevenShteinDistanceWithMaximum(String token1, String token2, int maxDist) {
        if (token1.length() == 0)
            return token2.length();
        if (token2.length() == 0)
            return token1.length();

        int token1Lenght = token1.length();
        int[] column = new int[token1Lenght + 1];

        int index2 = 0;
        char ch2 = token2.charAt(index2);
        column[0] = 1;

        for (int index1 = 1; index1 <= token1Lenght; index1++) {
            int cost = token1.charAt(index1 - 1) == ch2 ? 0 : 1;
            column[index1] = Math.min(column[index1 - 1], index1 - 1) + cost;
        }

        int top = 0;
        for (index2 = 1; index2 < token2.length(); index2++) {
            ch2 = token2.charAt(index2);
            top = index2 + 1;
            int minimum = token1Lenght * 2;
            for (int ix1 = 1; ix1 <= token1Lenght; ix1++) {
                int cost = token1.charAt(ix1 - 1) == ch2 ? 0 : 1;

                int value = Math.min(Math.min(top, column[ix1 - 1]), column[ix1]) +
                        cost;
                column[ix1 - 1] = top;
                top = value;
                minimum = Math.min(minimum, value);
            }
            column[token1Lenght] = top;

            //because of definition fo maximum, we have to return max, when minimum of calculation exceed the max
            if (minimum > maxDist)
                return maxDist + 1;
        }

        return top;
    }

    //Question Four
    private void runTestsBySingleColumnLevenShteinWithMaximum() {
        ArrayList<TestCaseModel> arrayList = getTestCases();
        for (TestCaseModel test : arrayList) {
            System.out.println("Distance:" + OneColumnLevenShteinDistanceWithMaximum(test.getToken1(), test.getToken2(), test.getMaximum()) + "- Expect:" + test.getExpectReturnWithMax());
        }
    }

    //Question Five 1/4
    private void singleColumnLevenShteinMemoryMeasurment() {
        ArrayList<TestCaseModel> arrayList = getTestCases();
        for (TestCaseModel test : arrayList) {
            Runtime runtime = Runtime.getRuntime();
            long usedMemoryBefore = runtime.totalMemory() - runtime.freeMemory();
            OneColumnLevenShteinDistance(test.getToken1(), test.getToken2());
            long usedMemoryAfter = runtime.totalMemory() - runtime.freeMemory();
            System.out.println("Memory increased: " + (usedMemoryAfter - usedMemoryBefore) + " bytes");
        }
    }

    //Question Five 2/4
    private void singleColumnLevenShteinWithMaximumMemoryMeasurment() {
        ArrayList<TestCaseModel> arrayList = getTestCases();
        for (TestCaseModel test : arrayList) {
            Runtime runtime = Runtime.getRuntime();
            long usedMemoryBefore = runtime.totalMemory() - runtime.freeMemory();
            OneColumnLevenShteinDistanceWithMaximum(test.getToken1(), test.getToken2(), test.getMaximum());
            long usedMemoryAfter = runtime.totalMemory() - runtime.freeMemory();
            System.out.println("Memory increased: " + (usedMemoryAfter - usedMemoryBefore) + " bytes");
        }
    }



    int iterateNumber = 10000000;

    //Question Five 3/4
    private void singleColumnLevenShteinTimeMeasurment() {
        ArrayList<TestCaseModel> arrayList = getTestCases();
        Long time = System.currentTimeMillis();
        for (TestCaseModel test : arrayList) {
            for (int i = 0; i < iterateNumber; i++) {
                OneColumnLevenShteinDistance(test.getToken1(), test.getToken2());
            }
            System.out.println("Time spend on case:" + test.getToken1() + "And" + test.getToken2() + ": " + ((double) (System.currentTimeMillis() - time)) / (double) iterateNumber);
            time = System.currentTimeMillis();
        }
    }

    //Question Five 4/4
    private void singleColumnLevenShteinWithMaximumTimeMeasurment() {
        ArrayList<TestCaseModel> arrayList = getTestCases();
        Long time = System.currentTimeMillis();
        for (TestCaseModel test : arrayList) {
            for (int i = 0; i < iterateNumber; i++) {
                OneColumnLevenShteinDistanceWithMaximum(test.getToken1(), test.getToken2(), test.getMaximum());
            }
            System.out.println("Time spend on case:" + test.getToken1() + " And " + test.getToken2() + ": " + ((double)(System.currentTimeMillis() - time)) /(double) iterateNumber);
            time = System.currentTimeMillis();
        }
    }

    private ArrayList<TestCaseModel> getTestCases() {
        ArrayList<TestCaseModel> arrayList = new ArrayList<>();
        TestCaseModel testCaseModel1 = new TestCaseModel("Haus", "Maus", 1, 1, 1);
        TestCaseModel testCaseModel2 = new TestCaseModel("Haus", "Mausi", 2, 2, 2);
        TestCaseModel testCaseModel3 = new TestCaseModel("Haus", "Häuser", 3, 3, 3);
        TestCaseModel testCaseModel4 = new TestCaseModel("Kartoffelsalat", "Runkelrüben", 2, 12, 3);
        arrayList.add(testCaseModel1);
        arrayList.add(testCaseModel2);
        arrayList.add(testCaseModel3);
        arrayList.add(testCaseModel4);
        return arrayList;
    }

}
